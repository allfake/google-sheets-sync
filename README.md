#Readme

Google Sheets Sync is tool for sync data from google spreadsheet
for unity 5.x only in Editor (that mean you can't use this in Runtime).

Now we use Google Sheets API v3.

### How to use

Create new GameObject.

Add component Google Sheets Sync.

![Add component Google Sheets Sync
](https://bytebucket.org/allfake/google-sheets-sync/raw/59006b98cee8eecf13cf0ac2967667853f9c1262/Readme%20Image/1.png "Add component Google Sheets Sync
")

Click Setup Credentials.

![Click Setup Credentials
](https://bytebucket.org/allfake/google-sheets-sync/raw/59006b98cee8eecf13cf0ac2967667853f9c1262/Readme%20Image/2.png "Click Setup Credentials
")

Go to google sheet api

[https://developers.google.com/sheets/quickstartdotnet](https://developers.google.com/sheets/quickstart/dotnet)


Follow Step 1 [set up wizard](https://console.developers.google.com/start/api?id=sheets.googleapis.com).

After that you will get **Client Id** and **Client Secret**

![See in goole Client Id and Client Secret
](https://bytebucket.org/allfake/google-sheets-sync/raw/59006b98cee8eecf13cf0ac2967667853f9c1262/Readme%20Image/3.png "See in goole Client Id and Client Secret
")

In scope filed fill **https://spreadsheets.google.com/feeds**

![Client Id and Client Secret
](https://bytebucket.org/allfake/google-sheets-sync/raw/59006b98cee8eecf13cf0ac2967667853f9c1262/Readme%20Image/4.png "Client Id and Client Secret
")

Click **Get Access Code**

![Click Get Access Code
](https://bytebucket.org/allfake/google-sheets-sync/raw/59006b98cee8eecf13cf0ac2967667853f9c1262/Readme%20Image/5.png "Click Get Access Code
")

Now you will see **Access Token**, click **Save & Close**

![Client Id and Client Secret
](https://bytebucket.org/allfake/google-sheets-sync/raw/59006b98cee8eecf13cf0ac2967667853f9c1262/Readme%20Image/6.png "Client Id and Client Secret
")

Go to your google spreadsheet in **your google drive**.

Look at url of your **link**.

Copy your sheet id.

Find sheet id => https://docs.google.com/spreadsheets/d/**[This is your sheet id]**/

Type **title** of sheet that your want [ex. title is **Readme**].

![See title in goole sheet
](https://bytebucket.org/allfake/google-sheets-sync/raw/59006b98cee8eecf13cf0ac2967667853f9c1262/Readme%20Image/7.png "See title in goole sheet
")

Fill **Sheet id** and **Sheet name** in component.

If your sheet has **header** then **check** in **Skip fist row**.

Click **Sync Sheet**.

You will see your data below.

___

### Script

___


Get Data

```
GoogleSheetsSyncData.GetCellData(string title, int rowIndex)
```

Get Row count

```
GoogleSheetsSyncData.RowCount
```
___

Example Class

```
public class MyClassCanGetDataFromGoogleSheet : MonoBehaviour {

	void Start () {
		GoogleSheetsSyncData googleSheetsSyncData = gameObject.GetComponent<GoogleSheetsSyncData>();

		for (int i = 0; i < googleSheetsSyncData.RowCount; i ++) {
			string myData = googleSheetsSyncData.GetCellData("MyColumnName", i);

			Debug.Log(myData);
		}

	}
}
```