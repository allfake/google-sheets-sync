﻿using UnityEngine;
using UnityEditor;

using System.Collections.Generic;

using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;


[CustomEditor(typeof(GoogleSheetsSync))]
public class GoogleSheetsSyncEditor : Editor
{
	private const float FixedColumnWidth = 50f;

	private GoogleSheetsSync m_Component;
	private Vector3 m_ScrollPosition;

	private OAuth2Parameters m_OAuth2Parameters;
	private GOAuth2RequestFactory m_OAuth2RequestFactory;
	private SpreadsheetsService m_SpreadsheetsService;

	private SerializedProperty m_ScriptProperty;
	private SerializedProperty m_DataProperty;
	private SerializedProperty m_DataSheetIdProperty;
	private SerializedProperty m_DataSheetNameProperty;
	private SerializedProperty m_DataSkipFirstRowProperty;

	private bool m_ShowTable = true;
	private bool m_ShowData = false;

	private void OnEnable()
	{
		m_Component = target as GoogleSheetsSync;
		m_ScriptProperty = serializedObject.FindProperty("m_Script");
		m_DataProperty = serializedObject.FindProperty("Data");
		m_DataSheetIdProperty = m_DataProperty.FindPropertyRelative("SheetId");
		m_DataSheetNameProperty = m_DataProperty.FindPropertyRelative("SheetName");
		m_DataSkipFirstRowProperty = m_DataProperty.FindPropertyRelative("SkipFirstRow");
	}

	public override void OnInspectorGUI()
	{
		DrawProperties();
		DrawTableData();
		DrawButtons();
	}

	private void DrawProperties()
	{
		serializedObject.Update();
		EditorGUILayout.PropertyField(m_ScriptProperty);
		EditorGUILayout.PropertyField(m_DataSheetIdProperty);
		EditorGUILayout.PropertyField(m_DataSheetNameProperty);
		EditorGUILayout.PropertyField(m_DataSkipFirstRowProperty);
		serializedObject.ApplyModifiedProperties();
	}

	private void DrawTableData()
	{
		// Display table data.
		m_ScrollPosition = EditorGUILayout.BeginScrollView(m_ScrollPosition, GUILayout.ExpandHeight(false));

		EditorGUILayout.BeginHorizontal();
		foreach (string title in m_Component.Data.Titles)
		{
			EditorGUILayout.LabelField(title, EditorStyles.boldLabel, GUILayout.MinWidth(0f), GUILayout.Width(FixedColumnWidth));
		}
		EditorGUILayout.EndHorizontal();

		foreach (GoogleSheetsSyncData.Row row in m_Component.Data.Rows)
		{
			EditorGUILayout.BeginHorizontal();
			foreach (string value in row.Cells)
			{
				EditorGUILayout.LabelField(value, GUILayout.MinWidth(0f), GUILayout.Width(FixedColumnWidth));
			}
			EditorGUILayout.EndHorizontal();
		}

		EditorGUILayout.EndScrollView();
	}

	private void DrawButtons()
	{
		EditorGUILayout.BeginHorizontal();
		GUI.enabled = IsReadyToSync;
		if (GUILayout.Button("Sync Sheet"))
		{
			SyncSheet();
		}
		GUI.enabled = true;
		GUILayout.FlexibleSpace();
		if (GUILayout.Button("Setup Credentials"))
		{
			GoogleAppCredentialsEditorWindow.OpenWindow();
		}
		EditorGUILayout.EndHorizontal();
	}

	private void SyncSheet()
	{
		if (m_OAuth2Parameters == null)
		{
			m_OAuth2Parameters = new OAuth2Parameters();
		}
		m_OAuth2Parameters.ClientId = EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.ClientIdKey);
		m_OAuth2Parameters.ClientSecret = EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.ClientSecretKey);
		m_OAuth2Parameters.RedirectUri = EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.RedirectUriKey);
		m_OAuth2Parameters.Scope = EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.ScopeKey);
		m_OAuth2Parameters.AccessCode = EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.AccessCodeKey);
		m_OAuth2Parameters.AccessToken = EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.AccessTokenKey);
		m_OAuth2Parameters.RefreshToken = EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.RefreshTokenKey);

		WorksheetFeed worksheetFeed = GetWorksheetFeed();
		WorksheetEntry worksheetEntry = null;
		foreach (WorksheetEntry entry in worksheetFeed.Entries)
		{
			if (entry.Title.Text == m_Component.Data.SheetName)
			{
				worksheetEntry = entry;
				break;
			}
		}

		if (worksheetEntry == null)
		{
			EditorUtility.DisplayDialog("Worksheet not found", string.Format("Worksheet named \"{0}\" could not be found.", m_Component.Data.SheetName), "OK");
			return;
		}

		m_Component.Data.Clear();

		int numColumns = 0;

		// Headers.
		CellQuery cellQuery = new CellQuery(worksheetEntry.CellFeedLink);
		CellFeed cellFeed = m_SpreadsheetsService.Query(cellQuery);
		List<string> firstRowTitles = new List<string>();

		foreach (CellEntry cell in cellFeed.Entries)
		{
			if (cell.Row == 1)
			{
				if (m_Component.Data.SkipFirstRow)
				{
					m_Component.Data.Titles.Add(cell.Value);
				}
				else
				{
					m_Component.Data.Titles.Add(cell.Title.Text.Substring(0, cell.Title.Text.Length - 1));
					firstRowTitles.Add(cell.Value);

				}
				numColumns++;
			}
		}

		if (firstRowTitles.Count > 0)
		{
			GoogleSheetsSyncData.Row row = new GoogleSheetsSyncData.Row();
			foreach (string title in firstRowTitles)
			{
				row.Cells.Add(title);
			}
			m_Component.Data.Rows.Add(row);
		}

		// Data rows.
		AtomLink listFeedLink = worksheetEntry.Links.FindService(GDataSpreadsheetsNameTable.ListRel, null);
		ListQuery listQuery = new ListQuery(listFeedLink.HRef.ToString());
		ListFeed listFeed = m_SpreadsheetsService.Query(listQuery);

		foreach (ListEntry entry in listFeed.Entries)
		{
			GoogleSheetsSyncData.Row row = new GoogleSheetsSyncData.Row();
			for (int i = 0; i < entry.Elements.Count; i++)
			{
				ListEntry.Custom element = entry.Elements[i];
				row.Cells.Add(element.Value);

				if (i == numColumns - 1)
				{
					break;
				}
			}
			m_Component.Data.Rows.Add(row);
		}
	}

	private string SpreadsheetUri
	{
		get { return GoogleAppCredentialsEditorWindow.SpreadsheetFeedApiUriPrefix + m_Component.Data.SheetId; }
	}

	private WorksheetFeed GetWorksheetFeed()
	{
		return GetWorksheetFeed(SpreadsheetUri);
	}

	private WorksheetFeed GetWorksheetFeed(string uri)
	{
		OfflineCertificationValidator.Start();
		if (m_OAuth2RequestFactory == null)
		{
			m_OAuth2RequestFactory = new GOAuth2RequestFactory(null, "", m_OAuth2Parameters);
			m_SpreadsheetsService = new SpreadsheetsService("");
			m_SpreadsheetsService.RequestFactory = m_OAuth2RequestFactory;
		}

		SpreadsheetQuery query = new SpreadsheetQuery();
		query.Uri = new System.Uri(uri);

		SpreadsheetFeed feed = m_SpreadsheetsService.Query(query);
		SpreadsheetEntry spreadsheet = (SpreadsheetEntry)feed.Entries[0];

		WorksheetFeed worksheetFeed = spreadsheet.Worksheets;
		OfflineCertificationValidator.Stop();

		return worksheetFeed;
	}

	private bool IsReadyToSync
	{
		get
		{
			bool hasAccessCode = !string.IsNullOrEmpty(EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.AccessCodeKey));
			bool hasAccessToken = !string.IsNullOrEmpty(EditorPrefs.GetString(GoogleAppCredentialsEditorWindow.AccessTokenKey));
			return hasAccessCode && hasAccessToken;
		}
	}
}
